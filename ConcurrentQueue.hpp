#pragma once

#include "StdAliases.h"
#include "TypesUtils.hpp"
#include "Logger.hpp"

#include <queue>
#include <list>
#include <variant>

namespace Concurrency
{

template <typename T>
class ConcurrentQueue final
{
public:
    void push(T&& value)
    {
        bool was_empty{false};

        {
            LockGuard lk(_mutex);

             was_empty = _queue.empty();

            _queue.push_back(FWD(value));
        }

        if (was_empty)
        {
            _cv.notify_one();
        }
    }

    T pop()
    {
        UniqueLock lk(_mutex);

//        LOGX("Queue waiting on cv");
        _cv.wait(lk, [this] () { return !_queue.empty(); });
//        LOGX("Queue has data");

        T value = std::move(_queue.front());

        _queue.pop_front();

        lk.unlock();

        return value;
    }

    std::size_t size() const
    {
        LockGuard lk(_mutex);

        return _queue.size();
    }

    template <typename F>
    void traverse(F&& fn)
    {
        {
            LockGuard lk(_mutex);

            for (const auto& value : _queue)
            {
                fn(value);
            }
        }
    }

private:
    std::list<T> _queue;

    mutable Mutex _mutex;

    CondVar _cv;
};

} // namespace Concurrency
