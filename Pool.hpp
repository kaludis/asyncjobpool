#pragma once

#include "Worker.hpp"
#include "Observer.hpp"
#include "ConcurrentMessageBus.hpp"
#include "Messages.hpp"
#include "Aux.hpp"

#include "Logger.hpp"

#include <cassert>

#include <queue>
#include <vector>
#include <algorithm>
#include <list>

namespace Jobs
{

template <typename... JobTypes>
class Pool
{
public:
    using ThisType = Pool<JobTypes...>;

    using WorkerType = Worker<ThisType, JobTypes...>;
    using WorkerRef = std::unique_ptr<WorkerType>;

    using JobAuxSumType = std::variant<Aux<JobTypes>...>;
    using ObserverRefSumType = std::variant<ObserverRef<typename Aux<JobTypes>::DataType, ThisType>...>;

    using WorkerList = std::vector<WorkerRef>;
    using JobQueue = std::list<JobAuxSumType>;
    using JobList = std::list<JobAuxSumType>;

    using MaybeWorkerIt = Maybe<typename WorkerList::iterator>;

    // Bus messages

    using NewJobMsgSum = std::variant<NewJobMessage<Aux<JobTypes>>...>;

    using YieldJobMsgSum = std::variant<YieldJobMessage<Aux<JobTypes>>...>;
    using DoneJobMsgSum = std::variant<DoneJobMessage<Aux<JobTypes>>...>;

    using ResumeJobMsg = ResumeJobMessage<std::size_t>;
    using CancelJobMsg = CancelJobMessage<std::size_t>;

    // Bus

    using MessageBus = Concurrency::ConcurrentMessageBus
                        <
                            NewJobMsgSum,
                            YieldJobMsgSum,
                            DoneJobMsgSum,
                            ResumeJobMsg,
                            CancelJobMsg,
                            WorkerFreeMsg,
                            ShutdownPoolMsg
                        >;

    using MsgSumType = typename MessageBus::MsgSumType;

public:
    Pool(std::size_t size)
        : _msg_polling_thread(&Pool::messagePollingRoutine, this)
    {
        _workers.reserve(size);

        for (std::size_t i = 0ul; i < size; ++i)
        {
            _workers.push_back(std::make_unique<WorkerType>(this, i));
        }
    }

    template
    <
        typename JobType,
        typename JobObserverRef = ObserverRef<typename Aux<JobType>::DataType, ThisType>
    >
    JobObserverRef addJob(JobType&& job)
    {
        Aux<JobType> job_aux(FWD(job));

        JobObserverRef observer = job_aux.observer(this);

        post(NewJobMsgSum(NewJobMessage<Aux<JobType>>{std::move(job_aux)}));

        return observer;
    }

    ~Pool()
    {
        shutdown();

        //printInternalStatus();
    }

    void shutdown()
    {
        if (!_finished)
        {
            post(ShutdownPoolMsg{});

            for (WorkerRef& worker : _workers)
            {
                worker->waitForDone();
            }

            _msg_polling_thread.join();
        }
    }

    void post(MsgSumType&& msg)
    {
        const std::string msg_str = msgToString(msg);
        //LOGX("Post message '", msg_str, "'");
        _bus.post(FWD(msg));
        //LOGX("Message '", msg_str, "' posted");
    }

private:
    void messagePollingRoutine()
    {
        ////LOGC("Pool message polling routine started");

        while (!_finished)
        {
            using namespace TypeUtils;

            _bus.dispatch
            (
                [this] (NewJobMsgSum&& sum_msg) {
                    std::visit(
                        [this] (auto&& msg) {
                            auto job_aux = std::move(msg.data);

                            bool worker_found{false};

                            //LOGX("Message NewJobMsg[", msg.data.id(), "]: Try to dispatch to worker");

                            for (auto& worker : _workers)
                            {
                                if (worker->doJob(std::move(job_aux)))
                                {
                                    worker_found = true;
                                    ////LOGX("Message NewJobMsg[", msg.data.id(), "]: Dispatched to worker");
                                    break;
                                }
                            }

                            if (!worker_found)
                            {
                                //LOGX("Message NewJobMsg[", job_aux.id(), "] dispatched to queue");
                                _job_queue.push_back(std::move(job_aux));
                            }

                            printInternalStatus();
                        }
                    , FWD(sum_msg));
                },
                [this] (YieldJobMsgSum&& sum_msg) {
                    //LOGX("Message YieldJobMsg[", getId(sum_msg), "] dispatched");

                    std::visit(
                        [this] (auto&& msg) {
                            auto job_aux = std::move(msg.data);

                            auto it = _yielded_jobs.insert(std::end(_yielded_jobs), std::move(job_aux));

                            std::visit([] (auto& job_aux) { job_aux.notifyObserver(); }, *it);
                        }
                    , FWD(sum_msg));
                },
                [this] (DoneJobMsgSum&& sum_msg) {
                    //LOGX("Message DoneJobMsg[", getId(sum_msg), "] dispatched");

                    std::visit(
                        [this] (auto&& msg) {
                            auto job_aux = std::move(msg.data);
                            job_aux.notifyObserver();
                            // Last scope for completed job
                            //printInternalStatus();
                        }
                    , FWD(sum_msg));
                },
                [this] (ResumeJobMsg&& msg) {
                    const std::size_t job_index = msg.data;
                    //LOGX("Message ResumeJobMsg[", job_index, "] dispatched");

                    auto it = std::find_if(std::begin(_yielded_jobs), std::end(_yielded_jobs),
                        [job_index] (const JobAuxSumType& aux_sum) {
                            return std::visit([job_index] (const auto& aux) { return aux.id() == job_index; }, aux_sum);
                        });

                    if (it != std::end(_yielded_jobs))
                    {
                        JobAuxSumType aux_sum = std::move(*it);

                        _yielded_jobs.erase(it);

                        std::visit([this] (auto&& aux) {
                            using JobAuxType = std::decay_t<decltype(aux)>;
                            post(NewJobMsgSum(NewJobMessage<JobAuxType>{FWD(aux)}));
                        }, std::move(aux_sum));

                        _resumed.push_back(job_index);
                    }
                    else
                    {
                        if (Contains(_resumed, job_index))
                        {
                            //LOGX("Job already resumed! ", job_index);
                            assert(false);
                        }
                    }
                },
                [this] (CancelJobMsg&& msg) {
                    const std::size_t job_index = msg.data;

                    //LOGX("Message CancelJobMsg[", job_index, "] dispatched");

                    auto it = std::find_if(std::begin(_yielded_jobs), std::end(_yielded_jobs),
                        [job_index] (const JobAuxSumType& aux_sum) {
                            return std::visit([job_index] (const auto& aux) { return aux.id() == job_index; }, aux_sum);
                        });

                    if (it != std::end(_yielded_jobs))
                    {
                        _yielded_jobs.erase(it);
                    }
                },
                [this] (WorkerFreeMsg&& msg) {
                    if (!_job_queue.empty())
                    {
                        const std::size_t index = msg.data;

                        auto& worker = _workers[index];

                        auto job_aux_sum = std::move(_job_queue.front());

                        _job_queue.pop_front();

                        std::visit([this, &worker, index] (auto&& aux) {
                            using AuxType = std::decay_t<decltype(aux)>;

                            post(NewJobMsgSum(NewJobMessage<AuxType>{std::move(aux)}));
                        }, std::move(job_aux_sum));
                    }
                    else
                    {
                        //LOGX("No jobs for worker");
                        //printInternalStatus();
                    }
                },
                [this] (ShutdownPoolMsg&&) { _finished = true; }
            );
        }
    }

    MaybeWorkerIt findFreeWorker()
    {
        auto it = std::find_if(std::begin(_workers), std::end(_workers),
            [] (const WorkerRef& worker) {
            return worker->isFree();
        });

        return (it != std::end(_workers)) ? Just(std::move(it)) : Nothing;
    }

    void printInternalStatus()
    {
        //LOGX("===== Internal pool status =====");
        //LOGX("Job queue size: ", _job_queue.size());
        //LOGX("Yilded job list size: ", _yielded_jobs.size());

        //LOGC("Msg queue size: ", _bus.size());

        //LOGC("Messages types:");

//        _bus.traverse(
//            [] (const NewJobMsgSum&) { //LOGC("NewJobMsgSum"); },
//            [] (const YieldJobMsgSum&) { //LOGC("YieldJobMsgSum"); },
//            [] (const DoneJobMsgSum&) { //LOGC("DoneJobMsgSum"); },
//            [] (const ResumeJobMsg&) { //LOGC("ResumeJobMsg"); },
//            [] (const CancelJobMsg&) { //LOGC("CancelJobMsg"); },
//            [] (const WorkerFreeMsg&) { //LOGC("WorkerFreeMsg"); },
//            [] (const ShutdownPoolMsg&) { //LOGC("ShutdownPoolMsg"); }
//        );

        ////LOGX("================================");
    }

    std::string msgToString(const MsgSumType& msg_sum) const
    {
        return std::visit(TypeUtils::Overload(
          [this] (const NewJobMsgSum& msg_sum) { return std::string("NewJobMsg[").append(std::to_string(getId(msg_sum))).append("]"); },
          [this] (const YieldJobMsgSum& msg_sum) { return std::string("YieldJobMsg[").append(std::to_string(getId(msg_sum))).append("]"); },
          [this] (const DoneJobMsgSum& msg_sum) { return std::string("DoneJobMsg[").append(std::to_string(getId(msg_sum))).append("]"); },
          [] (const ResumeJobMsg& msg) { return std::string("ResumeJobMsg[").append(std::to_string(msg.data)).append("]"); },
          [] (const CancelJobMsg& msg) { return std::string("CancelJobMsg[").append(std::to_string(msg.data)).append("]"); },
          [] (const WorkerFreeMsg& msg) { return std::string("WorkerFreeMsg[").append(std::to_string(msg.data)).append("]"); },
          [] (const ShutdownPoolMsg&) { return std::string("ShutdownPoolMsg"); }
        ), msg_sum);
    }

    template<typename T>
    std::size_t getId(const T& msg_sum) const
    {
        return std::visit([] (const auto& msg) { return msg.data.id(); }, msg_sum);
    }

    bool Contains(const std::vector<std::size_t>& v,std::size_t id)
    {
        auto it = std::find(std::begin(v), std::end(v), id);

        return it != std::end(v);
    }

private:
    std::thread _msg_polling_thread;

    std::vector<std::size_t> _resumed;

    WorkerList _workers;

    JobQueue _job_queue;

    JobList _yielded_jobs;

    MessageBus _bus;

    bool _finished{false};
};

} // namespace Jobs
