#pragma once

#include "ConcurrentQueue.hpp"

#include "TypesUtils.hpp"

#include <variant>
#include <functional>
#include <utility>

namespace Concurrency
{

template <typename... Ts>
class ConcurrentMessageBus final
{
public:
    using MsgSumType = std::variant<Ts...>;

private:
    using OverloadedType = TypeUtils::Overloaded<std::function<void (Ts&&)>...>;

    template <typename... Fn>
    struct TraverseFunctor
    {
        using OverloadType = TypeUtils::Overloaded<Fn...>;

        TraverseFunctor(Fn&&... fn)
            : _overload{FWD(fn)...}
        {}

        template <typename T>
        void operator ()(const T& value) const
        {
            std::visit(_overload, value);
        }

        OverloadType _overload;
    };

public:
    template <typename T>
    void post(T&& msg)
    {
        _queue.push(FWD(msg));
    }

    template <typename... Fn>
    void dispatch(Fn&&... fn)
    {
        std::visit(TypeUtils::Overload(FWD(fn)...), _queue.pop());
    }

    template <typename... Fn>
    void traverse(Fn&&... fn)
    {
        _queue.traverse(TraverseFunctor(FWD(fn)...));
    }

    std::size_t size() const
    {
        return _queue.size();
    }

private:
    ConcurrentQueue<MsgSumType> _queue;
};

} // namespace Concurrency
