#pragma once

#include "Messages.hpp"

#include <iostream>
#include <chrono>
#include <thread>

struct FastJobData {};
using FastJobResult = Jobs::JobResult<FastJobData>;

class FastJob final
{
public:

    using ResultType = FastJobResult;

    FastJobResult operator ()() const
    {
        //std::cout << "I am Fast Job: STARTED" << std::endl;
        using namespace std::chrono_literals;

        std::this_thread::sleep_for(1ms);

        //std::cout << "I am Fast Job: COMPLETE" << std::endl;

        return Jobs::Done(FastJobData{});
    }
};

struct NormalJobData {};
using NormalJobResult = Jobs::JobResult<NormalJobData>;

class NormalJob final
{
public:

    using ResultType = NormalJobResult;

    NormalJobResult operator ()() const
    {
        //std::cout << "I am Normal Job: STARTED" << std::endl;
        using namespace std::chrono_literals;

        std::this_thread::sleep_for(10ms);
        //std::cout << "I am Normal Job: COMPLETE" << std::endl;

        return Jobs::Done(NormalJobData{});
    }
};

struct SlowJobData {};
using SlowJobResult = Jobs::JobResult<SlowJobData>;

class SlowJob final
{
public:

    using ResultType = SlowJobResult;

    SlowJobResult operator ()() const
    {
        if (!_yilded)
        {
            //std::cout << "I am Slow Job: STARTED" << std::endl;
            using namespace std::chrono_literals;

            std::this_thread::sleep_for(5ms);
            //std::cout << "I am Slow Job: YIELD" << std::endl;

            _yilded = true;

            return Jobs::Yield(SlowJobData{});
        }
        else
        {
            //std::cout << "I am Slow Job: RESUMED" << std::endl;
            using namespace std::chrono_literals;

            std::this_thread::sleep_for(5ms);
            //std::cout << "I am Slow Job: DONE" << std::endl;

            _yilded = true;

            return Jobs::Done(SlowJobData{});
        }
    }

private:
    mutable bool _yilded{false};
};

