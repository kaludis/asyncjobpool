/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Logger.hpp
 * Author: kaludis
 *
 * Created on August 16, 2017, 6:34 PM
 */

#pragma once

#include "LoggerStream.h"

#include <cstdio>

#include <string>
#include <sstream>
#include <iostream>                                                             
#include <thread>

template <typename T>                                                           
std::string logger_helper(const T& t)                                                  
{                                                                               
	std::stringstream ss;
	ss << t << " ";
	return ss.str();
}                                                                               
                                                                                
template <typename T, typename... Args>                                         
std::string logger_helper(const T& t, const Args&... args)                             
{       
	return logger_helper(t) + logger_helper<Args...>(args...);
}                                                                               
                                                                                
template <typename... Args>                                                     
void logger(const char* file, const char* fun, int line, const Args&... args)
{
	std::stringstream ss;
	ss << std::this_thread::get_id() << " | ";

	std::string s;
	s.append("[").append("#").append(ss.str()).append(file).append(":").append(fun).append("():").append(std::to_string(line)).append("] ");
	s.append(logger_helper(args...)).append("\n");

    LoggerStream::stream() << s;
    LoggerStream::stream().flush();
}                                                                               

template <typename... Args>                                                     
void loggerc(const char* file, const char* fun, int line, const Args&... args)
{
	std::stringstream ss;
	ss << std::this_thread::get_id() << " | ";

	std::string s;
	s.append("[").append("#").append(ss.str()).append(file).append(":").append(fun).append("():").append(std::to_string(line)).append("] ");
	s.append(logger_helper(args...)).append("\n");

    fprintf(stdout, "%s", s.c_str());
}                                                                               

template <typename... Args>                                                     
void loggerx(const char* file, const char* fun, int line, const Args&... args)
{
	std::stringstream ss;
	ss << std::this_thread::get_id() << " | ";

	std::string s;
	s.append("[").append("#").append(ss.str()).append(file).append(":").append(fun).append("():").append(std::to_string(line)).append("] ");
	s.append(logger_helper(args...)).append("\n");

    fprintf(stdout, "%s", s.c_str());

    LoggerStream::stream() << s;
    LoggerStream::stream().flush();
}                                                                               

//        fprintf(stdout, "%s:%s():%d: " fmt, __FILE__, __func__, __LINE__, __VA_ARGS__);

#define LOGGING

#ifdef LOGGING

#define LOGF(...) \
    do { \
        logger(__FILE__, __func__, __LINE__, __VA_ARGS__); \
    } while (0);

#define LOGC(...) \
    do { \
        loggerc(__FILE__, __func__, __LINE__, __VA_ARGS__); \
    } while (0);

#define LOGX(...) \
    do { \
        loggerx(__FILE__, __func__, __LINE__, __VA_ARGS__); \
    } while (0);

#else

#define LOGF(...)

#define LOGC(...)

#define LOGX(...)

#endif // LOGGING
