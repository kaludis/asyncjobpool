**Naive implementation of async job pool with thread pool and base 
yield/resume support**


Requirements: C++17 (tested on gcc 8.2.1 and clang).


**What is it?**

It is a naive implementation of async job pool with base yield/resume support.


**Central entities:**

- ADT (Algebraic Data Types or Sum Types based on std::variant)

- ADT Pattern Mathing (based on std::visit and some fold expression magic)

- Concurrent Message Bus (bus for message-based event dispatching)

- Thread Pool (pool of std::thread)

- Job (callable object)

- Observer (object for job status observe purpose)

