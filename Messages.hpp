#pragma once

#include "TypesUtils.hpp"

#include <cstddef>

#include <variant>

namespace Jobs
{

template <typename T, typename Tag>
struct Message
{
    T data;
};


struct NewJobTag;

template <typename T>
using NewJobMessage = Message<T, NewJobTag>;


struct ResumeJobTag;

template <typename T>
using ResumeJobMessage = Message<T, ResumeJobTag>;


struct CancelJobTag;

template <typename T>
using CancelJobMessage = Message<T, CancelJobTag>;


struct WorkerFreeTag;

template <typename T>
using WorkerFreeMessage = Message<T, WorkerFreeTag>;

using WorkerFreeMsg = WorkerFreeMessage<std::size_t>;


struct YieldJobTag;

template <typename T>
using YieldJobMessage = Message<T, YieldJobTag>;


struct DoneJobTag;

template <typename T>
using DoneJobMessage = Message<T, DoneJobTag>;


enum class JobResultState
{
    Yield,
    Done
};

template <typename T>
struct JobResult
{
    using DataType = T;

    JobResultState state;
    DataType data;
};

template <typename T>
inline
JobResult<T> Yield(T&& data)
{
    return {JobResultState::Yield, FWD(data)};
}

template <typename T>
inline
JobResult<T> Done(T&& data)
{
    return {JobResultState::Done, FWD(data)};
}

struct ShutdownPoolMsg {};

} // namespace Jobs
