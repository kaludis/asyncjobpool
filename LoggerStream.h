#pragma once

#include <fstream>
#include <string>

class LoggerStream final {
public:
	LoggerStream(const std::string& file_name);

	LoggerStream(const LoggerStream&) = delete;

	LoggerStream& operator =(const LoggerStream&) = delete;

	LoggerStream(LoggerStream&&) = delete;

	LoggerStream& operator =(LoggerStream&&) = delete;

	~LoggerStream() = default;

	static void init(const std::string& file_name);

	static std::ofstream& stream();

private:
	std::ofstream _ofs;

	static std::string _file_name;
};
