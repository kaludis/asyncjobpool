#include "LoggerStream.h"

#include <cstdio>

LoggerStream::LoggerStream(const std::string& file_name)
	: _ofs{file_name, std::fstream::out | std::fstream::trunc}
{
    if (!_ofs.is_open())
    {
        printf("Failed to open log file '%s'\n", file_name.c_str());
    }
}

void LoggerStream::init(const std::string& file_name)
{
	LoggerStream::_file_name = file_name;
}

std::ofstream& LoggerStream::stream()
{
	static LoggerStream logger_stream{LoggerStream::_file_name};

	return logger_stream._ofs;
}

std::string LoggerStream::_file_name{};
