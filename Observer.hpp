#pragma once

#include "ControlBlock.hpp"
#include "TypesUtils.hpp"
#include "Messages.hpp"
#include "Logger.hpp"

namespace Jobs
{

using namespace TypeUtils;

template <typename Result, typename Pool>
class Observer
{
public:
    using ResumeJobMsg = ResumeJobMessage<std::size_t>;
    using CancelJobMsg = CancelJobMessage<std::size_t>;

    Observer(std::size_t job_id, const JobControlRef<Result>& control, Pool* pool)
        : _job_id(job_id),
          _control(control),
          _pool(pool)
    {}

    Observer(const Observer&) = delete;

    Observer& operator =(const Observer&) = delete;

    Observer(Observer&&) = default;

    Observer& operator =(Observer&&) = default;

    Maybe<Result> tryGet()
    {
        if (_control->hasData())
        {
            return Just(std::move(_control->data()));
        }
        else
        {
            return Nothing;
        }
    }

    Maybe<Result> get()
    {
        _control->wait();

        return tryGet();
    }

    void resume()
    {
        _control->recharge();

//        LOGX("Observer[", _job_id, "] send ResumeJobMsg[", _job_id, "]");
        _pool->post(ResumeJobMsg{_job_id});
    }

    void cancel()
    {
        _pool->post(CancelJobMsg{_job_id});
    }

    std::size_t id() const
    {
        return _job_id;
    }

private:
    const std::size_t _job_id;

    JobControlRef<Result> _control;

    Pool* _pool{nullptr};
};

template <typename Result, typename Pool>
using ObserverRef = std::unique_ptr<Observer<Result, Pool>>;

} // namespace Jobs
