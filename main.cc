#include "TypesUtils.hpp"
#include "JobSamples.h"
#include "Pool.hpp"
#include "ConcurrentMessageBus.hpp"
#include "Messages.hpp"
#include "Logger.hpp"

#include <ctime>
#include <cstdlib>

#include <variant>
#include <functional>
#include <map>
#include <thread>
#include <iterator>

using JobSumType = std::variant<FastJob, NormalJob, SlowJob>;

JobSumType JobFactory(std::size_t job_index)
{
    static const std::map<std::size_t, std::function<JobSumType ()>> m
    {
        {1ul, [] () { return FastJob(); }},
        {2ul, [] () { return NormalJob(); }},
        {3ul, [] () { return SlowJob(); }}
    };

    return m.at(job_index)();
}

template <typename T>
bool TryCheckResult(const T& jobserver)
{
    const auto result = jobserver->tryGet();

    if (result)
    {
        return true;
    }
    else
    {
        return false;
    }
}

template <typename T>
void CheckResult(const T& jobserver)
{
    const auto result = jobserver->get();
}

template <typename T>
void CheckYieldResumeResult(const T& jobserver)
{
    const auto result = jobserver->get();

    if (result)
    {
        jobserver->resume();

        const auto result2 = jobserver->get();
    }
    else
    {

    }
}

template <typename T>
void CheckYieldCancelResult(const T& jobserver)
{
    const auto result = jobserver->get();

    if (result)
    {
        jobserver->cancel();
    }
    else
    {

    }
}

using JobPool = Jobs::Pool<FastJob, NormalJob, SlowJob>;
using ObserverRefSumType = typename JobPool::ObserverRefSumType;
using ObserverTypePair = std::pair<std::size_t, ObserverRefSumType>;
using ObserverList = std::vector<ObserverTypePair>;


void StartJob(JobPool& pool, std::size_t job_index)
{
    static int i = 1;

    TypeUtils::Match
    (
        JobFactory(job_index),
        [&] (FastJob&& job) { CheckResult(pool.addJob(FWD(job))); },
        [&] (NormalJob&& job) { CheckResult(pool.addJob(FWD(job))); },
        [&] (SlowJob&& job) {
            (i % 2 == 0) ?
                CheckYieldResumeResult(pool.addJob(FWD(job))) :
                CheckYieldCancelResult(pool.addJob(FWD(job)));
        }
    );

    ++i;
}

template <typename T>
ObserverRefSumType MakeObserverRefSum(T&& value)
{
    return ObserverRefSumType(FWD(value));
}

struct Observer
{
    void get() {}

    void tryGet() {}
};

struct ResumableObserver
{
    void get() {}

    void tryGet() {}

    void resume() {}

    void cancel() {}
};

using ObserverSum = SumType<Observer, ResumableObserver>;

struct Job {};

struct ResumableJob {};

struct MyJob1 : public Job
{
    void operator ()() const {}
};

struct MyJob2 : public ResumableJob
{
    void operator ()() const {}
};

using JobSum = SumType<MyJob1, MyJob2>;

struct Result{};

ObserverSum DoJob(JobSum&& job_sum)
{
    ObserverSum result;

    TypeUtils::Match
    (
        FWD(job_sum),
        [&] (MyJob1&&) { std::cout << "Observer" << std::endl; result = Observer{}; },
        [&] (MyJob2&&) { std::cout << "ResumableObserver" << std::endl; result = ResumableObserver{}; }
    );

    return result;
}



int main(void)
{
    std::srand(std::time(nullptr));

    LoggerStream::init("Jobs.log");

    LOGC("Start");

    constexpr std::size_t thread_pool_size = 4ul;
    constexpr std::size_t jobs_per_thread = 100ul;
    constexpr std::size_t job_pool_workers = 2ul;

    std::cout << "Press [Enter] to start" << std::endl;
    std::cin.get();

    auto ob1 = DoJob(MyJob1{});

    TypeUtils::Match
    (
        ob1,
        [] (Observer& ob) { std::cout << "Observer" << std::endl; ob.tryGet(); ob.get(); },
        [] (ResumableObserver& ob) { std::cout << "ResumableObserver" << std::endl; ob.tryGet(); ob.get(); ob.resume(); ob.cancel(); }
    );

    auto ob2 = DoJob(MyJob2{});

    TypeUtils::Match
    (
        ob2,
        [] (Observer& ob) { std::cout << "Observer" << std::endl; ob.tryGet(); ob.get(); },
        [] (ResumableObserver& ob) { std::cout << "ResumableObserver" << std::endl; ob.tryGet(); ob.get(); ob.resume(); ob.cancel(); }
    );

//#define TEST

#ifdef TEST

    LOGX("Immediately result reuest test");

    {
        std::vector<std::thread> tp;
        tp.reserve(thread_pool_size);

        JobPool jpool(job_pool_workers);

        for (std::size_t i = 0ul; i < thread_pool_size; ++i)
        {
            tp.emplace_back([&] () {
                const auto id = std::this_thread::get_id();

                LOGX("Thread ", id, " started");

                for (std::size_t j = 0ul; j < jobs_per_thread; ++j)
                {
                    const std::size_t a = std::rand() % 3 + 1;

                    StartJob(jpool, a);
                }

                LOGX("Thread ", id, " completed");
            });
        }

        for (auto& t : tp)
        {
            t.join();
        }
    }

    LOGX("Deferred result reuest test");

    {
        std::vector<std::thread> tp;
        tp.reserve(thread_pool_size);

        JobPool jpool(job_pool_workers);

        for (std::size_t i = 0ul; i < thread_pool_size; ++i)
        {
            tp.emplace_back([&] () {
                const auto id = std::this_thread::get_id();

                ObserverList ob_lst;
                ob_lst.reserve(jobs_per_thread);

                LOGX("Thread ", id, " started");

                for (std::size_t j = 0ul; j < jobs_per_thread; ++j)
                {
                    const std::size_t a = std::rand() % 3 + 1;

                    TypeUtils::Match
                    (
                        JobFactory(a),
                        [&] (auto&& job) {
                             auto ob = jpool.addJob(FWD(job));
                             ob_lst.emplace_back(a, MakeObserverRefSum(std::move(ob)));
                        }
                    );
                }

                LOGX("Thread ", id, ": all jobs placed in pool");

                for (const auto& p : ob_lst)
                {
                    const std::size_t job_index = p.first;

                    TypeUtils::Match
                    (
                        p.second,
                        [&] (auto& ob) {
                            switch (job_index)
                            {
                            case 1:
                            case 2:
                                ob->get();
                                break;
                            case 3:
                                ob->get();
                                ob->resume();
                                ob->get();
                                break;
                            }
                        }
                    );
                }

                LOGX("Thread ", id, ": all observers returns results");
            });
        }

        for (auto& t : tp)
        {
            t.join();
        }
    }
#endif // TEST

    LOGC("End");

    return 0;
}



