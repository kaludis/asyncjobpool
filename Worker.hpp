#pragma once

#include "Aux.hpp"
#include "Messages.hpp"
#include "Logger.hpp"

#include <cassert>

#include <variant>
#include <thread>
#include <atomic>

namespace Jobs
{

template <typename PoolType, typename... JobTypes>
class Worker
{
public:
    using JobAuxSumType = std::variant<Aux<JobTypes>...>;

    using JobRef = std::unique_ptr<JobAuxSumType>;

    using YieldJobMsgSum = std::variant<YieldJobMessage<Aux<JobTypes>>...>;
    using DoneJobMsgSum = std::variant<DoneJobMessage<Aux<JobTypes>>...>;

    Worker(PoolType* pool, std::size_t id)
        : _pool(pool),
          _id(id),
          _thr(&Worker::run, this)
    {}

    template
    <
        typename JobAuxType,
        typename ResultType = typename JobAuxType::ResultType
    >
    bool doJob(JobAuxType&& job_aux)
    {
        _mutex.lock();

        const bool is_free = _job.get() == nullptr ? true : false;

        if (is_free)
        {
            _job = std::make_unique<JobAuxSumType>(std::move(job_aux));

//            LOGX("Worker[", _id, "] picks up Job[", job_aux.id(), "]");

            _mutex.unlock();

            _cv.notify_one();

            return true;
        }
        else
        {
            _mutex.unlock();

            return false;
        }
    }

    void waitForDone()
    {
        if (!_finished.load())
        {
            _finished.store(true);

            _cv.notify_one();

            _thr.join();
        }
    }

    std::size_t id() const
    {
        return _id;
    }

private:
    void run()
    {
//        LOGC("Worker ", _id, " thread started");

        while (!_finished.load())
        {
            UniqueLock lk(_mutex);

            _cv.wait(lk, [this] () { return (_job.get() != nullptr) || _finished.load(); });

            if (!_job)
            {
                break;
            }

            JobAuxSumType& job_sum = *_job;

            const JobResultState state = std::visit(Overload(
                [] (auto& job) { return job(); }
            ), job_sum);

            std::visit(Overload(
                [this, state] (auto&& job) {
                    using JobAuxType = std::decay_t<decltype(job)>;

                    switch (state)
                    {
                    case JobResultState::Yield:
                        _pool->post(YieldJobMsgSum(YieldJobMessage<JobAuxType>{std::move(job)}));
                        break;
                    case JobResultState::Done:
                        _pool->post(DoneJobMsgSum(DoneJobMessage<JobAuxType>{std::move(job)}));
                        break;
                    }
                }
            ), job_sum);

            _job.reset(nullptr);

            lk.unlock();

            _pool->post(WorkerFreeMsg{_id});
        }
    }

private:
    PoolType* _pool{nullptr};

    const std::size_t _id;

    mutable std::thread _thr;

    mutable Mutex _mutex;

    CondVar _cv;

    JobRef _job{nullptr};

    std::atomic_bool _finished{false};
};

} // namespace Jobs
