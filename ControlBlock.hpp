#pragma once

#include "StdAliases.h"
#include "TypesUtils.hpp"
#include "Logger.hpp"

#include <atomic>
#include <memory>

namespace Jobs
{

template <typename T>
class ControlBlock
{
public:
    ControlBlock(std::size_t id) : _id(id) {}

    void wait()
    {
        UniqueLock lk(_mutex);

//        LOGX("CB[", _id, "] wait for data");
        _cv.wait(lk, [this] () {
            return _readiness.load();
        });

//        LOGX("CB[", _id, "] has data");

        lk.unlock();
    }

    void notify()
    {
//        LOGX("CB[", _id, "] notify data");

        _readiness.store(true);

        _cv.notify_one();
    }

    void setData(T&& data)
    {
        {
//            LOGX("CB[", _id, "] set data");

            LockGuard lk(_mutex);

            _data = std::move(data);
        }
    }

    T data()
    {
        return _data;
    }

    bool hasData() const
    {
        return _readiness.load();
    }

    void recharge()
    {
        _readiness.store(false);
    }

private:
    const std::size_t _id;

    CondVar _cv;

    Mutex _mutex;

    std::atomic_bool _readiness{false};

    T _data;
};

template <typename T>
using JobControlRef = std::shared_ptr<ControlBlock<T>>;

} // namespace Jobs
