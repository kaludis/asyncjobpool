#pragma once

#include <mutex>
#include <condition_variable>
#include <variant>
#include <utility>

using Mutex = std::mutex;

using CondVar = std::condition_variable;

using LockGuard = std::lock_guard<std::mutex>;

using UniqueLock = std::unique_lock<std::mutex>;

template <typename... Ts>
using SumType = std::variant<Ts...>;

#define FWD(...) std::forward<decltype(__VA_ARGS__)>(__VA_ARGS__)
