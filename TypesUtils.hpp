#pragma once

#include "StdAliases.h"

#include <utility>

#define EXPERIMENTAL_OPTIONAL 1

#ifdef EXPERIMENTAL_OPTIONAL
  #include <experimental/optional>
#else
  #include <optional>
#endif

namespace TypeUtils
{

template <typename T>
struct AggregateAdapter : public T
{
    template <typename... Args>
    AggregateAdapter(Args&&... args) : T{std::forward<Args>(args)...} {}
};

template<typename... Fs>
struct Overloaded : Fs...
{
    template<typename... Ts>
    Overloaded(Ts&&... args) : Fs{std::forward<Ts>(args)}... {}

    using Fs::operator()...;
};

template<typename... Ts>
Overloaded<Ts...> Overload(Ts&&... args)
{
    return Overloaded<Ts...>{std::forward<Ts>(args)...};
}

template <typename T, typename... Ts>
inline
void Match(T&& value, Ts&&... args)
{
    std::visit(Overload(FWD(args)...), FWD(value));
}

class Identifiable
{
public:
    Identifiable(std::size_t id) : _id(id) {}

    std::size_t id() const { return _id; }

private:
    const std::size_t _id;
};

#ifdef EXPERIMENTAL_OPTIONAL
    template <typename T>
    using Maybe = std::experimental::optional<T>;

    inline constexpr std::experimental::nullopt_t Nothing{std::experimental::nullopt_t::_Construct::_Token};

    template <typename T>
    inline
    Maybe<T> Just(T&& value) { return std::experimental::make_optional(std::forward<T>(value)); }
#else
    template <typename T>
    using Maybe = std::optional<T>;

    inline constexpr std::nullopt_t Nothing{std::nullopt_t::__secret_tag{}, std::nullopt_t::__secret_tag{}};

    template <typename T>
    inline
    Maybe<T> Just(T&& value) { return std::make_optional(std::forward<T>(value)); }
#endif

} // namespace TypeUtils
